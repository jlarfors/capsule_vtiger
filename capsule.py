import pycurl
# from lxml import etree
from StringIO import StringIO
from config import capsule_url
from config import capsule_token


def get_open_opps():
    buffer = StringIO()
    c = pycurl.Curl()
    c.setopt(c.URL, capsule_url + '/api/opportunity?status=open')
    c.setopt(c.USERPWD, capsule_token)
    c.setopt(c.WRITEDATA, buffer)
    c.perform()
    c.close()

    return buffer.getvalue()


def get_closed_opps():
    buffer = StringIO()
    c = pycurl.Curl()
    c.setopt(c.URL, capsule_url + '/api/opportunity?status=closed')
    c.setopt(c.USERPWD, capsule_token)
    c.setopt(c.WRITEDATA, buffer)
    c.perform()
    c.close()

    return buffer.getvalue()


def get_opp_details(opp_id):
    buffer = StringIO()
    c = pycurl.Curl()
    c.setopt(c.URL, capsule_url + '/api/opportunity/' + opp_id)
    c.setopt(c.USERPWD, capsule_token)
    c.setopt(c.WRITEDATA, buffer)
    c.perform()
    c.close()

    return buffer.getvalue()
