#!/usr/bin/python

# VTiger
user_name = "dirk.honisch"
# look at my preferences in vtiger webinterface
user_access_key = ""
# url to vtiger webservice
vtiger_url = "http://emenda-fr.dyndns.org:8092/vtigerCRM/webservice.php"
# Vtiger company name were all capsule opps will be related to
company_name = "Emenda Switzerland"

# Capsule
capsule_url = "https://emenda.capsulecrm.com"
# look at my preferences -> API Authentication Token in capsule webinterface
capsule_token = ":x"

# Misc
# today - delta_days = date for closed opps to sync 
delta_days = 90
