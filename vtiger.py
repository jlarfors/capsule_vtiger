#!/usr/bin/python

import hashlib
import urllib
import json
import pycurl
try:
    # python 3
    from urllib.parse import urlencode
except ImportError:
    # python 2
    from urllib import urlencode
from StringIO import StringIO
from lxml import etree
from datetime import datetime, timedelta
from config import vtiger_url
from config import user_name
from config import user_access_key
from config import company_name
from config import delta_days
from capsule import get_opp_details


def get_challenge_token():
    buffer = StringIO()
    c = pycurl.Curl()
    c.setopt(c.URL,
             vtiger_url
             + '?operation=getchallenge&username='
             + user_name)
    c.setopt(c.WRITEDATA, buffer)
    c.perform()
    c.close()

    parsed_json = json.loads(buffer.getvalue())
    return parsed_json['result']['token']


def get_session(challenge_token):
    # create md5sum from user_access_key and challange_token
    m = hashlib.md5()
    m.update(challenge_token + user_access_key)
    md5_sum = m.hexdigest()

    # login into vtiger and get session_id and user_id
    buffer = StringIO()
    c = pycurl.Curl()
    c.setopt(c.URL, vtiger_url)
    post_data = {'operation': 'login',
                 'username': user_name,
                 'accessKey': md5_sum}
    postfields = urlencode(post_data)
    c.setopt(c.POSTFIELDS, postfields)
    c.setopt(c.WRITEDATA, buffer)
    c.perform()
    c.close()

    parsed_json = json.loads(buffer.getvalue())
    session_name = parsed_json['result']['sessionName']
    user_id = parsed_json['result']['userId']
    return [session_name, user_id]


def get_company_id(session):
    buffer = StringIO()
    c = pycurl.Curl()
    query = urllib.quote("select * from Accounts where accountname='"
                         + company_name + "';")
    url_emenda = (vtiger_url
                  + "?operation=query&query="
                  + query + "&sessionName="
                  + session[0])

    c.setopt(c.URL, url_emenda)
    c.setopt(c.WRITEDATA, buffer)
    c.perform()
    c.close()

    parsed_json = json.loads(buffer.getvalue())
    result_list = parsed_json["result"]
    if len(result_list) != 1:
        print "None or more than one company name found!"
        exit(0)

    return parsed_json["result"][0]["id"]


def get_opps(session, company_id):
    buffer = StringIO()
    c = pycurl.Curl()
    query = urllib.quote("select * from Potentials where related_to='"
                         + company_id + "';")
    url_emenda = (vtiger_url
                  + "?operation=query&query="
                  + query + "&sessionName=" + session[0])

    c.setopt(c.URL, url_emenda)
    c.setopt(c.WRITEDATA, buffer)
    c.perform()
    c.close()

    return buffer.getvalue()


def list_opps(result):
    parsed_json = json.loads(result)
    opps = parsed_json["result"]
    print "Opportunity Names:"
    for opp in opps:
        print opp['potential_no'] + " " + opp['potentialname']
    return 0


def delete_opps(session, result):
    parsed_json = json.loads(result)
    opps = parsed_json["result"]

    if len(opps) < 1:
        print "No opps in list, exit!"
        return 0

    for opp in opps:
        # login into vtiger and delete all opps
        buffer = StringIO()
        c = pycurl.Curl()
        c.setopt(c.URL, vtiger_url)

        post_data = {'sessionName': session[0],
                     'operation': 'delete',
                     'id': opp['id']}
        postfields = urlencode(post_data)
        c.setopt(c.POSTFIELDS, postfields)
        c.setopt(c.WRITEDATA, buffer)
        c.perform()
        c.close()

    parsed_json = json.loads(buffer.getvalue())
    print parsed_json["result"]["status"]
    return 0


def create_opp(session, json_opp):

    buffer = StringIO()
    c = pycurl.Curl()
    c.setopt(c.URL, vtiger_url)
    post_data = {'sessionName': session[0],
                 'operation': 'create',
                 'element': json_opp,
                 'elementType': 'Potentials'}
    postfields = urlencode(post_data)
    c.setopt(c.POSTFIELDS, postfields)
    c.setopt(c.WRITEDATA, buffer)
    c.perform()
    c.close()

    parsed_json = json.loads(buffer.getvalue())
    return parsed_json["success"]


def parse_and_create_opps(opps_string, company_id, session):
    open_opps = etree.fromstring(opps_string)
    for opp in open_opps.iter("opportunity"):
        potentialname = ""
        closingdate = ""
        related_to = company_id
        assigned_user_id = session[1]
        sales_stage = ""
        description = ""
        amount = ""
        actual_close_date = ""
        opp_id = ""

        for element in opp.iter("*"):
            if element.tag == "id":
                opp_id = element.text

            if element.tag == "name":
                potentialname = element.text

            if element.tag == "actualCloseDate" and element.text is not None:
                actual_close_date = element.text

            if element.tag == "description" and element.text is not None:
                description = element.text

            if element.tag == "value" and element.text is not None:
                amount = int(float(element.text))

            if element.tag == "probability" and element.text is not None:
                probability = int(float(element.text))
                if probability == 0:
                    sales_stage = "Closed Lost"
                elif probability <= 40:
                    sales_stage = "Prospecting"
                elif probability <= 90:
                    sales_stage = "Opportunity"
                elif probability == 100:
                    sales_stage = "Closed Won"

        create = ""
        if probability != 0 and probability != 100:
            create = True
        else:
            # check if the lost or won opp is within the config time frame
            since = datetime.today() - timedelta(days=delta_days)
            close_date = datetime.strptime(actual_close_date[:10], "%Y-%m-%d")
            if close_date >= since:
                create = True

        if create:
            # Get expected close date
            opp_details = etree.fromstring(get_opp_details(opp_id))
            xpath = opp_details.xpath("expectedCloseDate")
            if not xpath:
                xpath = opp_details.xpath("name")
                print "WARNING: Missing expectedCloseDate for opp: " + xpath[0].text
            else:
                closingdate = xpath[0].text[:10]

            json_opp_string = {'closingdate': closingdate,
                               'potentialname': potentialname,
                               'related_to': related_to,
                               'assigned_user_id': assigned_user_id,
                               'description': description,
                               'amount': amount,
                               'probability': probability,
                               'sales_stage': sales_stage}

            result = create_opp(session, json.dumps(json_opp_string))

            if result is True:
                print "Opp '" + potentialname + "' successfully created!"
            else:
                print result
